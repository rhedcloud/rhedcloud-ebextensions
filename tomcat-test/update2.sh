#! /bin/bash
TOMCAT_CONF=/Users/gwang28/project/temp3/server.xml
      CONFIGURED=`grep -c 'Connector port="8009" protocol="AJP/1.3" redirectPort="8443" secretRequired="false"' ${TOMCAT_CONF}`
      if [ $CONFIGURED = 0 ]
        then
          IS_TOMCAT50=`grep -c 'Connector port="8009" protocol="AJP/1.3" redirectPort="8443"' ${TOMCAT_CONF}`
          echo "IS_TOMCAT50=$IS_TOMCAT50"
          if [ $IS_TOMCAT50 = 0 ]
            then
              IS_TOMCAT51=`grep -c '<!-- Define an AJP 1.3 Connector on port 8009 -->' ${TOMCAT_CONF}`
              if [ $IS_TOMCAT51 = 0 ]
                then
                  echo "ERROR – Unknown version of Tomcat. Can’t configure AJP in Server.xml"
                  exit -1
              else
                echo "tomcat51"
                sed -i -e 's,<!-- Define an AJP 1.3 Connector on port 8009 -->,<Connector port="8009" protocol="AJP/1.3" redirectPort="8443" secretRequired="false"/>,' ${TOMCAT_CONF}
              fi
          else
            echo "tomcat50"
            sed -i -e 's,Connector port="8009" protocol="AJP/1.3" redirectPort="8443",Connector port="8009" protocol="AJP/1.3" redirectPort="8443" secretRequired="false",' ${TOMCAT_CONF}
          fi
          logger -t tomcat_conf "${TOMCAT_CONF} updated successfully..restarting tomcat"
          service tomcat8 restart
          exit 0
        else
          logger -t tomcat_conf "${TOMCAT_CONF} already updated"
          exit 0
      fi

